package com.krozz.listViewPerson;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater; import android.view.View;
import android.view.ViewGroup; import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView; import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuView;

import java.util.ArrayList;


public class CategoriaAdapter extends ArrayAdapter<ItemData> {

    int idGrupo;
    Activity Context;
    ArrayList<ItemData> list;
    ArrayList<ItemData> listFilter;
    LayoutInflater inflater;

    public CategoriaAdapter (Activity Context,int idGrupo,int id, ArrayList<ItemData> list)
    {

        super(Context,id,list);
        this.list=list;
        listFilter= list;
        inflater= (LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.idGrupo=idGrupo;
    }


    public View getView(int posicion,View convertView,ViewGroup parent)
    {
        View itemView= inflater.inflate(idGrupo,parent,false);

        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgSelect);
        imagen.setImageResource(list.get(posicion).getImageId());

        ImageView bandera = (ImageView) itemView.findViewById(R.id.imgbandera);
        bandera.setImageResource(list.get(posicion).getBanderaId());



        TextView textDescripcion=(TextView) itemView.findViewById(R.id.lbDescripcion);
        textDescripcion.setText(list.get(posicion).getTextDescripcion());

        TextView textPrecio=(TextView) itemView.findViewById(R.id.lbPrecio);
        textPrecio.setText(list.get(posicion).getTextPrecio());

        return itemView;
    }

    public View getDropDonwView(int position,View comverterView,ViewGroup parent)
    {
        return getView(position, comverterView, parent);
    }


    //buscar
    @Override
    public int getCount(){
        return list.size();
    }

    @Override
    public ItemData getItem(int pos){
        return list.get(pos);
    }
    @Override
    public long getItemId(int pos){
        return list.indexOf(getItem(pos));
    }
    @Override
    public Filter getFilter(){
        return new ListFilter();
    }

    private class ListFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results= new FilterResults();
            if(constraint!=null && constraint.length()>0){
                constraint= constraint.toString().toUpperCase();
                ArrayList<ItemData> arrayresul= new ArrayList<>();
                for(int x=0;x<listFilter.size();x++){
                    if(listFilter.get(x).getTextDescripcion().toUpperCase().contains(constraint)){
                        arrayresul.add(listFilter.get(x));
                    }
                }
                results.count = arrayresul.size();
                results.values= arrayresul;
            }
            else{
                results.count= listFilter.size();
                results.values= listFilter;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
         list = (ArrayList<ItemData>) results.values;
         notifyDataSetChanged();
        }
    }
}
