package com.krozz.listViewPerson;

public class ItemData {
    private Integer banderaId;
    private String textDescripcion;
    private String textPrecio;
    private Integer imageId;

    public ItemData(String text, Integer banderaId, Integer imageId,String precio) {
        this.banderaId = banderaId;
        this.textDescripcion = text;
        this.textPrecio= precio;
        this.imageId = imageId;
    }

    public Integer getBanderaId() {
        return banderaId;
    }

    public void setBanderaId(Integer banderaId) {
        this.banderaId = banderaId;
    }

    public String getTextPrecio() {
        return textPrecio;
    }

    public void setTextPrecio(String precio) {
        this.textPrecio = precio;
    }


    public String getTextDescripcion() {
        return textDescripcion;
    }

    public void setTextDescripcion(String text) {
        this.textDescripcion = text;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }
}
