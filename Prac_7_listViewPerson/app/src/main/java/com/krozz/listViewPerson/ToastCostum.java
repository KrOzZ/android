package com.krozz.listViewPerson;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ToastCostum {
    public  static void toastImage( Activity activity,ItemData categoria ){
        View view = activity.getLayoutInflater().inflate(R.layout.toast_imagen,(ViewGroup) activity.findViewById(R.id.parentview));
        ((TextView) view.findViewById(R.id.lbDescripcion)).setText(categoria.getTextDescripcion());
        ((ImageView) view.findViewById(R.id.imgSelect)).setImageResource(categoria.getImageId());

        Toast toast=new Toast(activity);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(view);
        toast.show();

    }
}
