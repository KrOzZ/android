package com.krozz.listViewPerson;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.support.v4.os.IResultReceiver;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listViews;
    CardView cardView;

    private SearchView srcLista;

    private ArrayList<String> arrayList;
    private CategoriaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] comidas= getResources().getStringArray(R.array.tipoComida);
        String[] precio= getResources().getStringArray(R.array.precioComida);


        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData(comidas[0], R.drawable.b_mexico, R.drawable.tacos,precio[0]));
        list.add(new ItemData(comidas[1], R.drawable.b_italia, R.drawable.pizza,precio[1]));
        list.add(new ItemData(comidas[2], R.drawable.b_japon, R.drawable.suchi,precio[2]));
        list.add(new ItemData(comidas[3], R.drawable.b_eeuu, R.drawable.papas,precio[3]));
        list.add(new ItemData(comidas[4], R.drawable.b_alemania, R.drawable.salchicha,precio[4]));


        listViews = (ListView) findViewById(R.id.lvCategoria);
        cardView= (CardView) findViewById(R.id.cardView);

        adapter = new CategoriaAdapter(this, R.layout.item_comida, R.id.cardView, list);
        listViews.setAdapter(adapter);
        
        cardView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.w("MSG", "Oliwis");
                ItemData itemData= (ItemData) parent.getItemAtPosition(position);
                ToastCostum.toastImage(MainActivity.this,itemData);
            }
        });
        
        /*listViews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.w("MSG", "Oliwis");
                ItemData itemData= (ItemData) parent.getItemAtPosition(position);
                ToastCostum.toastImage(MainActivity.this,itemData);
            }
        });
*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

}
