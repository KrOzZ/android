package com.krozz.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtnombre;
    private Button btnentrar;
    private Button btnsalir;
    private  Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtnombre = findViewById(R.id.txtnombre);
        btnentrar = findViewById(R.id.btnentrar);
        btnsalir = findViewById(R.id.btnsalir);
        rectangulo = new Rectangulo();

        btnentrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtnombre.getText().toString();

                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this,"Falta Ingresar Nombre",Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
                    intent.putExtra("nombre", nombre);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("rectangulo", rectangulo );

                    intent.putExtras(bundle);
                    startActivity(intent);

                }
            }
        });
        btnsalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
