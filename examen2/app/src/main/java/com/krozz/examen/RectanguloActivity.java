package com.krozz.examen;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.Serializable;

public class RectanguloActivity extends AppCompatActivity {


    private TextView lblnombre;

    private EditText txtancho;
    private EditText txtaltura;
    private TextView lblarea;
    private TextView lblperimetro;
    private Button btncalcular;
    private Button btnlimpiar;
    private Button btnregresar;

    private Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Rectangulo");
        setContentView(R.layout.rectangulo_layout);


        lblnombre = (TextView) findViewById(R.id.lblnombre);
        txtancho = (EditText) findViewById(R.id.txtancho);
        txtaltura = (EditText) findViewById(R.id.txtaltura);
        lblarea = (TextView) findViewById(R.id.lblarea);
        lblperimetro = (TextView) findViewById(R.id.lblperimetro);
        btncalcular = (Button) findViewById(R.id.btncalcular);
        btnlimpiar = (Button) findViewById(R.id.btnlimpiar);
        btnregresar = (Button) findViewById(R.id.btnregresar);



        //this.btncalcular.setOnClickListener(this.btncalcular());

        Bundle datos = getIntent().getExtras();
        lblnombre.setText("Nombre: " + datos.getString("nombre"));
        this.rectangulo=(Rectangulo) getIntent().getExtras().getSerializable("rectangulo");




        btncalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtaltura.getText().toString().matches("") || txtancho.getText().toString().matches("")){
                    Toast.makeText(RectanguloActivity.this, "Favor de ingresar valores correctos", Toast.LENGTH_SHORT).show();
                }else {

                    int ancho = Integer.valueOf(txtancho.getText().toString());
                    int altura = Integer.valueOf(txtaltura.getText().toString());
                    String valor = txtaltura.getText().toString();

                    rectangulo.setaltura(Integer.parseInt(valor));
                    rectangulo.setbase(ancho);


                    lblarea.setText("Area: " + rectangulo.calcularArea());
                    lblperimetro.setText("Perimetro: " + rectangulo.calcularPerimetro());
                }
            }
        });



        btnlimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblperimetro.setText("");
                lblarea.setText("");
                txtaltura.setText("");
                txtancho.setText("");
            }
        });

        btnregresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnlimpiar.performClick();

                finish();
            }
        });
    }
}
