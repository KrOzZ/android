package com.example.conversiones02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnImc;
    private Imc imc;
    private Conversiones conversion;
    private Button btnconver;
    private Button btnterminar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre = (EditText) findViewById(R.id.txtnombre);
        btnImc = (Button) findViewById(R.id.btnimc);
        btnconver = (Button) findViewById(R.id.btnconver);
        btnterminar = (Button) findViewById(R.id.btnterminar);

        imc = new Imc();
        conversion = new Conversiones();


        btnImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this, "Falto Capturar Nombre", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this, IMCActivity.class);
                    //Enviar un dato String
                    intent.putExtra("cliente", nombre);

                    //Enviar un objeto

                    Bundle objeto = new Bundle();
                    objeto.putSerializable("imc",imc);

                    intent.putExtras(objeto);

                    startActivity(intent);

                }
            }
        });

        btnconver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this, "Falto Capturar Nombre", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this, ConversionActivity.class);

                    intent.putExtra("cliente", nombre);

                    Bundle objeto = new Bundle();

                    objeto.putSerializable("conversion", conversion);

                    intent.putExtras(objeto);

                    startActivity(intent);
                }
            }
        });

        btnterminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
