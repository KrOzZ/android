package com.example.conversiones02;

import android.util.Log;

import java.io.Serializable;

public class Imc implements Serializable {
    private float alturametro;
    private float alturacentimetro;
    private float peso;
    //private float altura;

    public Imc(int alturametro, float alturacentimetro, float peso) {
        this.alturametro = alturametro;
        this.alturacentimetro = alturacentimetro;
        this.peso = peso;
    }

    public Imc(){

    }

    public float getAlturametro() {
        return alturametro;
    }

    public float getAlturacentimetro() {
        return alturacentimetro;
    }

    public float getPeso() {
        return peso;
    }

    public void setAlturametro(float alturametro) {
        this.alturametro = alturametro;
    }

    public void setAlturacentimetro(float alturacentimetro) {
        this.alturacentimetro = alturacentimetro;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float calcularimc (){
        float altura=0;
        if(this.alturametro>10) {
           altura  = (float) this.alturametro / 100;
        }
        else {
            altura= (float) this.alturametro;
        }
        Log.e("MSG", Float.toString((float) altura));

        //return (this.peso/((this.alturametro+(this.alturacentimetro/100)) * (this.alturametro+(this.alturacentimetro/100)) ));
        return  (this.peso/(altura * altura));
        //return this.alturacentimetro/100;
    }
}
