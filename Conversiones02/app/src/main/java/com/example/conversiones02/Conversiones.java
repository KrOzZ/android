package com.example.conversiones02;

import java.io.Serializable;

public class Conversiones implements Serializable {

    private float cantidad;

    public Conversiones(float cantidad) {
        this.cantidad = cantidad;
    }

    public Conversiones() {
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public float obtenergradoscelcius(){
        return (((this.cantidad-32)*5/9));
    }

    public float obtenergradosfaren(){
        return ((this.cantidad * 9/5)+32);
    }
}
