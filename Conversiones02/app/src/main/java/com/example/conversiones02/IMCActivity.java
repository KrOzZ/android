package com.example.conversiones02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class IMCActivity extends AppCompatActivity {
    private Imc IMC;
    private  TextView lblnombre;
    private  EditText txtmetros;
    //private EditText txtcentimetros;
    private  EditText txtkilos;

    private TextView lblresultado;
    private  TextView lblvalor;

    private  Button btncalcular;
    private Button btnlimpiar;
    private  Button btnregresar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);
        lblnombre = (TextView) findViewById(R.id.lblnombre);
        txtmetros = (EditText) findViewById(R.id.txtmetro);
        //txtcentimetros = (EditText) findViewById(R.id.txtcentimetros);
        txtkilos = (EditText) findViewById(R.id.txtpeso);
        lblresultado = (TextView) findViewById(R.id.lblresultado);
        lblvalor = (TextView) findViewById(R.id.lblvalor);
        btncalcular = (Button) findViewById(R.id.btncalcular);
        btnlimpiar = (Button) findViewById(R.id.btnlimpiar);
        btnregresar = (Button) findViewById(R.id.btnregresar);



        Bundle datos = getIntent().getExtras();


        lblnombre.setText("Nombre: " + (datos.getString("cliente")));

        IMC = (Imc) datos.getSerializable("imc");

        btncalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //|| txtcentimetros.getText().toString().matches("")
                if(txtmetros.getText().toString().matches("")  ||
                        txtkilos.getText().toString().matches("")){
                    Toast.makeText(IMCActivity.this, "Faltan campos", Toast.LENGTH_SHORT).show();
                }else{

                    IMC.setAlturametro(Float.valueOf(txtmetros.getText().toString()));
                    //IMC.setAlturacentimetro(Float.valueOf(txtcentimetros.getText().toString()));
                    IMC.setPeso(Float.valueOf(txtkilos.getText().toString()));

                    lblresultado.setText(String.valueOf(IMC.calcularimc()));

                    lblvalor.setText("Kg/m^2");


                }
            }
        });

        btnlimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtmetros.setText("");
                //txtcentimetros.setText("");
                txtkilos.setText("");
                lblvalor.setText("");
                lblresultado.setText("");
            }
        });

        btnregresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnlimpiar.performClick();

                Intent intent = new Intent( IMCActivity.this, MainActivity.class);

                startActivity(intent);

            }
        });



    }
}
