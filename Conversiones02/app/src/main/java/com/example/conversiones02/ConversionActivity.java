package com.example.conversiones02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ConversionActivity extends AppCompatActivity {
    private TextView lblnombre;
    private TextView lblcantidad;
    private Conversiones conversion;
    private EditText txtcalcular;
    private RadioButton rdcelsius;
    private RadioButton rdfaren;
    private Button btncalcular;
    private Button btnlimpiar;
    private Button btnregresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);
        lblnombre = (TextView) findViewById(R.id.lblnombre);
        txtcalcular = (EditText) findViewById(R.id.txtcantidad);
        rdcelsius = (RadioButton) findViewById(R.id.rdcelsiu);
        rdfaren = (RadioButton) findViewById(R.id.rdfaren);
        btncalcular = (Button) findViewById(R.id.btncambiar);
        btnlimpiar = (Button) findViewById(R.id.btnlimpiarconv);
        btnregresar= (Button) findViewById(R.id.btnback);
        lblcantidad = (TextView) findViewById(R.id.lblcantidad);

        Bundle datos = getIntent().getExtras();


        lblnombre.setText("Nombre: " + (datos.getString("cliente")));

        conversion = (Conversiones) datos.getSerializable("conversion");

        btncalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtcalcular.getText().toString().matches("")){
                    Toast.makeText(ConversionActivity.this, "Favor de llenar campos", Toast.LENGTH_SHORT).show();
                }else{
                    conversion.setCantidad(Float.valueOf(txtcalcular.getText().toString()));

                    if(rdcelsius.isChecked()){
                        //Farenheit a celsius
                        lblcantidad.setText("Resultado: " + String.valueOf(conversion.obtenergradoscelcius()));
                    }else if(rdfaren.isChecked()) {
                        lblcantidad.setText("Resultado: " + String.valueOf(conversion.obtenergradosfaren()));
                    }
                }
            }
        });

        btnlimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtcalcular.setText("");
                lblcantidad.setText("");
            }
        });

        btnregresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnlimpiar.performClick();
                Intent intent = new Intent(ConversionActivity.this, MainActivity.class);

                startActivity(intent);
            }
        });
    }
}
