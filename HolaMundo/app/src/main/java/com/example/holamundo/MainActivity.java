package com.example.holamundo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.icu.lang.UCharacterEnums;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Vibrator;


public class MainActivity extends AppCompatActivity {

    private Button Saludar;
    private Button Salir;
    private Button Limpiar;
    private EditText txtNombre;
    private TextView LbHello;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre= findViewById(R.id.txtnombre);
        LbHello= findViewById(R.id.LbHello);

        Saludar = findViewById(R.id.btnSaludar);
        Saludar.setOnClickListener(this.btnSaludarClick());
        Salir = findViewById(R.id.btnSalir);
        Salir.setOnClickListener(this.btnSalirClick());
        Limpiar = findViewById(R.id.btnLimpiar);
        Limpiar.setOnClickListener(this.btnLimpiarClick());


    }

    private View.OnClickListener btnSaludarClick()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    //deprecated in API 26
                    v.vibrate(500);
                }
                LbHello.setText("Que Onda Mundo :D !!");
                Toast.makeText(getApplicationContext(),"Hola "+ txtNombre.getText().toString()+ " ¿Como Estas?",Toast.LENGTH_SHORT).show();

            }
        };
    }
    private View.OnClickListener btnLimpiarClick()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtNombre.setText("");
                LbHello.setText("Hello word!!");


            }
        };
    }
    private View.OnClickListener btnSalirClick()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"adios "+ txtNombre.getText().toString(),Toast.LENGTH_SHORT).show();
                System.exit(0);


            }
        };
    }
}
