package com.krozz.prac_5_cotizacion;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

public class CotizacionActivity extends AppCompatActivity {

    private TextView lbvCliente;
    private TextView lbFolio;
    private EditText txtDescrip;
    private EditText txtValor;
    private EditText txtPago;
    private RadioGroup rbgPlazos;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lbTotal;
    private TextView lbEnganche;

    private Cotizacion cotizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        getSupportActionBar().setTitle("Cotizacion");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        lbvCliente = (TextView) findViewById(R.id.lbvCliente);
        lbFolio = (TextView) findViewById(R.id.lbFolio);
        txtDescrip = (EditText) findViewById(R.id.txtDescrip);
        txtValor = (EditText) findViewById(R.id.txtValor);
        txtPago = (EditText) findViewById(R.id.txtPago);
        rbgPlazos = (RadioGroup) findViewById(R.id.rbgPlazos);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        lbTotal = (TextView) findViewById(R.id.lbTotal);
        lbEnganche = (TextView) findViewById(R.id.lbEnganche);


        lbvCliente.setText(getString(R.string.cliente) + " " + getIntent().getExtras().getString("nombre"));
        this.cotizacion=(Cotizacion) getIntent().getExtras().getSerializable("cotizacion");
        lbFolio.setText(getString(R.string.folio) + " " + String.valueOf(cotizacion.getFolio()));

        rbgPlazos.setOnCheckedChangeListener(this.rbPlazosAction());
        this.btnCalcular.setOnClickListener(this.btnCalcularAction());
        this.btnLimpiar.setOnClickListener(this.btnLimpiarAction());
        this.btnRegresar.setOnClickListener(this.btnRegresarAction());
        this.cotizacion.setPlazos(12);

    }


    private RadioGroup.OnCheckedChangeListener rbPlazosAction()
    {
        return new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId)
                {
                    case R.id.rb12:
                        cotizacion.setPlazos(12);
                        break;
                    case R.id.rb18:
                        cotizacion.setPlazos(18);
                        break;
                    case R.id.rb24:
                        cotizacion.setPlazos(24);
                        break;
                    case R.id.rb36:
                        cotizacion.setPlazos(36);
                        break;
                }

            }
        };
    }
    private View.OnClickListener btnCalcularAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        };
    }

    public void calcular()
    {
        String descripcion = txtDescrip.getText().toString();
        String pagInicial = txtPago.getText().toString();
        String valor = txtValor.getText().toString();

        if (descripcion.matches("") || descripcion.matches("") ||
                pagInicial.matches("")) {
            Toast.makeText(getApplicationContext(),"Error Campos VACIOS",Toast.LENGTH_SHORT).show();
        }
        else
        {
            cotizacion.setDescripcion(descripcion);
            cotizacion.setPorEnganche(Float.parseFloat(pagInicial));
            cotizacion.setValorAuto(Float.parseFloat(valor));

            lbEnganche.setText(getString(R.string.enganche)  + " " +
                    dineroFormato(cotizacion.calcularEnganche()));
            lbTotal.setText(getString(R.string.total) + " " +
                    dineroFormato(cotizacion.calcularPagMensual()));

        }
    }

    private View.OnClickListener btnRegresarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
    }

    private View.OnClickListener btnLimpiarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtValor.setText("");
                txtPago.setText("");
                txtDescrip.setText("");
                lbTotal.setText(R.string.total);
                lbEnganche.setText(R.string.enganche);
                rbgPlazos.check(R.id.rb12);
            }
        };
    }
    private String dineroFormato(float valor)
    {
        return NumberFormat.getCurrencyInstance(new Locale("es", "MX")).format(valor);
    }



}
