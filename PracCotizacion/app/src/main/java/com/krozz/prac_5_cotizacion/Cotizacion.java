package com.krozz.prac_5_cotizacion;

import java.io.Serializable;
import java.util.Random;

public class Cotizacion implements Serializable {

    private int folio;
    private String descripcion;
    private float valorAuto;
    private float enganche;
    private int plazos;


    //constructor
    public Cotizacion(int folio,String descrip,float valorAuto,float enganche,int plazos){
        this.setFolio(this.generarFolio());
        this.setDescripcion(descrip);
        this.setValorAuto(valorAuto);
        this.setPorEnganche(enganche);
        this.setPlazos(plazos);
    }

    public Cotizacion()
    {
        this.folio = this.generarFolio();
    }

    //GETER AND SETTER
    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getPorEnganche() {
        return enganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.enganche = porEnganche;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    //comportamiento
    public int generarFolio()
    {
        return new Random().nextInt()%1000;
    }

    public float calcularEnganche()
    {
        return this.valorAuto * (this.enganche / 100);
    }

    public float calcularPagMensual()
    {
        float enganche = this.calcularEnganche();
        float totalF = this.valorAuto - enganche;
        return totalF / this.plazos;

    }
}
