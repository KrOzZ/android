package com.krozz.Prac_5_categoria;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater; import android.view.View;
import android.view.ViewGroup; import android.widget.ArrayAdapter;
import android.widget.ImageView; import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuView;

import java.util.ArrayList;


public class SpinnerAdapter extends ArrayAdapter<ItemData> {

    int idGrupo;
    Activity Context;
    ArrayList<ItemData> list;
    LayoutInflater inflater;

    public SpinnerAdapter (Activity Context,int idGrupo,int id, ArrayList<ItemData> list)
    {

        super(Context,id,list);
        this.list=list;
        inflater= (LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.idGrupo=idGrupo;
    }

    public View getView(int posicion,View convertView,ViewGroup parent)
    {
        View itemView= inflater.inflate(idGrupo,parent,false);
        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgImagen);
        imagen.setImageResource(list.get(posicion).getImageId());

        TextView textCategoria=(TextView) itemView.findViewById(R.id.lbCategorias);
        textCategoria.setText(list.get(posicion).getTextCategoria());


        TextView textDescripcion=(TextView) itemView.findViewById(R.id.lblDescripcion);
        textDescripcion.setText(list.get(posicion).getTextDescripcion());
        return itemView;
    }

    public View getDropDonwView(int position,View comverterView,ViewGroup parent)
    {
        return getView(position, comverterView, parent);
    }

}
